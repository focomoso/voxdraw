/**
 * voxdraw
 *
 * Releases:
 *
 * v0.01 uploaded to focomoso Oct. 12, 2011
 *
 * v0.02.nn
 *
 *
 * v0.03.nn
 * features:
	- draw lines between points while drawing (so we don't get ... . . .  .  .    .    .)
	- touch
 * issues:
	- when writing out the grid, make sure the png is wide enough for all the indexes (at least 2x...?)
		- I  thought I fixed this, but it seems to mess up sometimes...
	- bug in writing or reading grid (see '8 full planes.png')
	- color picker
	- more indexes?
	- scrollbars
		- on all 3 axese
	- rollover on tooltips
	- shadows
		- toggle none, rt, lf, both
		- toggle none, rt, lf, both
 * optimizations:
	- clip cube on save and import (including pos in the cube)
	- draw new voxes into the curr canvas only once rather than redrawing the entire draw plane
		- this will only work if there is in fact nothing in front of the new vox
 *
 * vLater:
  * features
	- use localStorage... to save the data array so the user can start where he left off
	- select region...
	- more zoom levels
		- use gridEvery to make the grid handle more sizes (currently hard coded to 4)
	- ability to "slice" the grid to see what's behind what (using the draw plane)
	- basic shapes
		- line
		- ellipsoid, rect
			- (use an offscreen image for the drawing plane and use built in canvas drawing methods
		- text
	- rotate
	- lighting
		- toggle direction
	- 3d view
		- use localStorage to send the data to a 3d version
		- think about doing the whole thing in webGL...
 * optimizations:
	- pack the voxGrid (set idx number to conserve memory)
	- save grid and canvas (and vox?) images for each state that's been built (ie, don't re build them every time)
		- can I cach them in localData somehow?
 */


// stringify functions that will be called from outside this file
// for closure compiler minification
window['main'] = main;
window['handleZoom'] = handleZoom;

// using: kinetic2d-v1.0.2.js
var drawKin, toolsKin;

var spriteList = []; // list of all the images that need to be loaded (and checked if they're ready)

// set dims
var voxDim = 10; // the size of the voxels on the screen (in pixels)
var centerPt = {x:0,y:0};
var gridEvery = 4;	// heavy grid line every n squares

// states
var planeSwitch = 2; // 0: x; 1: y; 2: z;
var planeVal = 0;		// how high the draw plane is in it's given axis
var fullRedraw = true;

var mode = "loading"; // possible modes: "loading", "rollover"; "drag"; "draw"; "snap"; "snapnogrid"  (todo: handle touch). Later: "slice" and draw modes
var drawIdx = 1;			// the currently selected vox index
var lastVoxPt = null;			// point3: the last vox drawn (so we can do a shift click line)
var windowId = 0; 		// counter so every save / snapshot is in a new window
var undone = false;	// true if we've just undone

var shiftDown = false;
var ctrlDown = false;
// var altDown = false;

// grids and drawing
var voxGrid = new VoxGrid(80,80,80,16); // the entire grid of voxes on or off screen
var lastGrid = new VoxGrid(80,80,80,16); // the last state for undo
var drawGrid = new DrawGrid(320/voxDim,320/voxDim,320/voxDim); // the dimensions of the portion of the voxGrid that's on screen

// voxes
var voxOffset = { x: -voxDim-1, y: -voxDim-1 };
var voxes = []; // list of Voxes

// actions
var actionsLow = new Sprite("img/actions.png", {x:0,y:0});
var totalActions = 8;
// @todo: add rollover with actionsHi

// images (these are all canvases used as offscreen images)
var toolCursor, cursor, emptyVox, gndImg, xPlaneImg,yPlaneImg,zPlaneImg;
var voxesBehind, voxesInfront, voxesCurr;

// later generate these from code

var xThumb 	= new Sprite("img/x-thumb.png",	{ x:  -11, 	y:  -6 	});
var yThumb 	= new Sprite("img/y-thumb.png",	{ x:  -22, 	y:  -11 	});
var zThumb 	= new Sprite("img/z-thumb.png",	{ x:  -11, 	y:  -16 	});

var toolsGnd = new Sprite("img/tools-gnd.png", { x: 0, y: 0 });

	// objects //

/** DrawGrid
 * Holds info about the grid we're displaying on the screen, but no data.
 * note: should never be larger than the VoxGrid
 * @constructor
 */
function DrawGrid(xDim,yDim,zDim) {
	this.xDim = xDim;
	this.yDim = yDim;
	this.zDim = zDim;
	this.x=0;
    this.y=0;
    this.z=0;

	this.clearBuffer = function(pSwitch) {
		switch (pSwitch) {
			case 0:	// x
				this.drawBuffer = new Array(this.yDim);
				for (var y = 0;y<this.yDim;y++)
					this.drawBuffer[y] = new Array(this.zDim);
				break;
			case 1:	// y
				this.drawBuffer = new Array(this.xDim);
				for (var x = 0;x<this.xDim;x++)
					this.drawBuffer[x] = new Array(this.zDim);
				break;
			case 2:	// z
				this.drawBuffer = new Array(this.xDim);
				for (var x = 0;x<this.xDim;x++)
					this.drawBuffer[x] = new Array(this.yDim);
				break;
		}
	}

	this.copyDrawPlane = function(pSwitch, pVal) {
		this.clearBuffer(pSwitch);
		switch (pSwitch) {
			case 0:	// x
				for (var y = 0; y<this.yDim;y++) {
					for (var z = 0; z<this.zDim;z++) {
						this.drawBuffer[y][z] = voxGrid.get(new Point3(pVal+this.x,y+this.y,z+this.z));
					}
				}
				break;
			case 1:	// y
				for (var x = 0; x<this.xDim;x++) {
					for (var z = 0; z<this.zDim;z++) {
						this.drawBuffer[x][z] = voxGrid.get(new Point3(x+this.x,pVal+this.y,z+this.z));
					}
				}
				break;
			case 2:	// z
				for (var x = 0; x<this.xDim;x++) {
					for (var y = 0; y<this.yDim;y++) {
						this.drawBuffer[x][y] = voxGrid.get(new Point3(x+this.x,y+this.y,pVal+this.z));
					}
				}
				break;
		}
	}

	this.clearDrawPlane = function(pSwitch, pVal) {
		switch (pSwitch) {
			case 0:	// x
				for (var y = 0; y<this.yDim;y++) {
					for (var z = 0; z<this.zDim;z++) {
						voxGrid.set(new Point3(pVal+this.x,y+this.y,z+this.z),0);
					}
				}
				break;
			case 1:	// y
				for (var x = 0; x<this.xDim;x++) {
					for (var z = 0; z<this.zDim;z++) {
						voxGrid.set(new Point3(x+this.x,pVal+this.y,z+this.z),0);
					}
				}
				break;
			case 2:	// z
				for (var x = 0; x<this.xDim;x++) {
					for (var y = 0; y<this.yDim;y++) {
						voxGrid.set(new Point3(x+this.x,y+this.y,pVal+this.z),0);
					}
				}
				break;
		}
	}

	this.pasteDrawPlane = function(pSwitch, pVal) {
		// todo: this will fail if the buffer is not the same shape as the current plane
		switch (pSwitch) {
			case 0:	// x
				for (var y = 0; y<this.yDim;y++) {
					for (var z = 0; z<this.zDim;z++) {
						if (this.drawBuffer[y][z] !=0)
							voxGrid.set(new Point3(pVal+this.x,y+this.y,z+this.z), this.drawBuffer[y][z]);
					}
				}
				break;
			case 1:	// y
				for (var x = 0; x<this.xDim;x++) {
					for (var z = 0; z<this.zDim;z++) {
						if (this.drawBuffer[x][z] !=0)
							voxGrid.set(new Point3(x+this.x,pVal+this.y,z+this.z), this.drawBuffer[x][z]) ;
					}
				}
				break;
			case 2:	// z
				for (var x = 0; x<this.xDim;x++) {
					for (var y = 0; y<this.yDim;y++) {
						if (this.drawBuffer[x][y] !=0)
							voxGrid.set(new Point3(x+this.x,y+this.y,pVal+this.z), this.drawBuffer[x][y]);
					}
				}
				break;
		}

	}
}
/** VoxGrid
 * Holds an array of 8 bit indexes.
 * This is the data only, no ref to how they're drawn (size, angle...)
 * 0: empty
 * @constructor
 */
function VoxGrid(xDim,yDim,zDim,maxIdx) {
	// todo: pack the array based on the maxIdx
	this.xDim = xDim;
	this.yDim = yDim;
	this.zDim = zDim;
	this.maxIdx = maxIdx;

	this.data = new Uint8Array(new ArrayBuffer(xDim*yDim*zDim)); // / maxIdx

	this.set = function(pt,val) {
		this.data[pt.x+pt.y*this.yDim+pt.z*this.yDim*this.zDim] = val; // / maxIdx
	}
	this.get = function(pt) {
		return this.data[pt.x+pt.y*this.yDim+pt.z*this.yDim*this.zDim]; // / maxIdx
	}
	this.replace = function(newGrid) {
		this.data = new Uint8Array(newGrid.data);
	}
	this.swap = function(swapGrid) {
		// swaps one grid's data with another (works either way)
		var tmpData = new Uint8Array(swapGrid.data);
		swapGrid.data = new Uint8Array(this.data);
		this.data = new Uint8Array(tmpData);
	}
	this.clear = function() {
		this.data = new Uint8Array(new ArrayBuffer(xDim*yDim*zDim)); // / maxIdx
	}
	this.print = function() {
		var str = "";
		for (var i = 0;i<this.data.length;i++)
			str += i + ":" + this.data[i] + "; ";
		console.log(str);
	}
}
/** Point2
 * A 2d point
 * note: not used
 * @constructor
 */
function Point2(x,y) {
	// a position in the vox grid.
	// (0,0,0) is in the middle (far, center corner)
	// x increases along the \ axis
	// y increases along the / axis
	// z increases along the | axis (z up)
	this.x = x;
	this.y = y;
}
/** Point3
 * A 3d point position in the vox grid.
 * (0,0,0) is in the middle (far, center corner)
 * x increases along the \ axis
 * y increases along the / axis
 * z increases along the | axis (z up)
 * @constructor
 */
function Point3(x,y,z) {
	this.x = x;
	this.y = y;
	this.z = z;
}
/** Color
 * @constructor
 */
function Color(r,g,b,a) {
	this.r = r;
	this.g = g;
	this.b = b;
	this.a = a;
	this.getHex = function () {
		return this.toHex(r)+this.toHex(g)+this.toHex(b);
	};
	this.getDark = function(val) {
		// returns a color with the same hue darkened to the given val (1.0 = original color; 0.0 = black)
		return new Color(this.r*val,this.g*val,this.b*val,this.a);
	};
	this.getDarkHex = function(val) {
		return this.getDark(val).getHex();
	};

	this.toHex = function(n) {
		n = parseInt(n,10);
		if (isNaN(n)) return "00";
		n = Math.max(0,Math.min(n,255));
		return "0123456789ABCDEF".charAt((n-n%16)/16)
		+ "0123456789ABCDEF".charAt(n%16);
	};
}
/** Sprite
 * simplest of sprite objects that takes an image and an offset into that image
 * @constructor
 */
function Sprite(src,offset) {
	this.src = src;
	this.offset = offset;
	this.img = new Image();
	this.img.ready = false;

	// add this to the spriteList (list of all sprites)
	spriteList.push(this);

	this.img.onload = function() {
		this.ready = true;
		checkImages();
	};

	this.init = function() {
		this.img.src = this.src;
	};
}
/** IData
 *	holds an offscreen canvas and its image data
 * @constructor
 */
function IData(w,h) {
	this.can = makeCanvas(w,h);
	this.ctx = this.can.getContext("2d");
	this.imgData = this.can.getContext("2d").createImageData(w,h);
	this.data = this.imgData.data;
}

	// utilities //

// mouseToVoxPoint()
function mouseToVoxPoint(mousePos,planeVal) {
	//	- converts mouse coordinates (x,y) to a Point3 on the grid given a plane
	//	- makes no attempt to constrain the output, will return negative or off grid numbers
	//	todo: translate
	var p3 = new Point3(0,0,0);
	var pt = {
		x: mousePos.x - centerPt.x,
		y: mousePos.y - centerPt.y
	};

	if (planeSwitch == 0) {									// x
		p3.x = planeVal;
		p3.y = Math.round((-pt.x-(voxDim/2)-1)/voxDim + planeVal);
		p3.z = Math.round((-pt.y-(voxDim/2))/voxDim-pt.x/(voxDim*2) + planeVal);
	} else if (planeSwitch == 1) {						// y
		p3.x = Math.round((pt.x-(voxDim/2)-1)/voxDim + planeVal);
		p3.y = planeVal;
		p3.z = Math.round((-pt.y-(voxDim/2))/voxDim+pt.x/(voxDim*2) + planeVal);
	} else {														// z
		p3.x = Math.round(pt.x/(voxDim*2)+(pt.y-(voxDim/2))/voxDim+planeVal);
		p3.y = Math.round((pt.y-(voxDim/2))/voxDim-pt.x/(voxDim*2)+planeVal);
		p3.z = planeVal;
	}
	return p3;
}

//	voxPointToCanvas()
function voxPointToCanvas(voxP) {
	//	- returns the middle of a vox point in canvas (x,y)
	//	todo: translate
	var pt = {
		x: centerPt.x,
		y: centerPt.y
	};

	// translate
	pt.x += voxP.x*voxDim - voxP.y * voxDim;
	pt.y += voxP.x*(voxDim/2) + voxP.y*(voxDim/2) + voxP.z* - voxDim;

	return (pt);
}

// constrainVox()
function constrainVox(vPt) {
	//	- makes sure a vox point falls inside our cube
	vPt.x = (vPt.x<0) ? 0 : vPt.x;
	vPt.x = (vPt.x>drawGrid.xDim-1) ? drawGrid.xDim-1 : vPt.x;
	vPt.y = (vPt.y<0) ? 0 : vPt.y;
	vPt.y = (vPt.y>drawGrid.yDim-1) ? drawGrid.yDim-1 : vPt.y;
	vPt.z = (vPt.z<0) ? 0 : vPt.z;
	vPt.z = (vPt.z>drawGrid.zDim-1) ? drawGrid.zDim-1 : vPt.z;
}

//	isOnDrawPlane()
function isOnDrawPlane(vPt) {
	//	- returns true if a vox point is on the current draw plane, false if not
	// make sure we're inside the entire cube
	if (vPt.x < 0) return false;
	if (vPt.x > drawGrid.xDim-1) return false;
	if (vPt.y < 0) return false;
	if (vPt.y > drawGrid.yDim-1) return false;
	if (vPt.z < 0) return false;
	if (vPt.z > drawGrid.zDim-1) return false;

	return true;
}

// checkImages()
function checkImages() {
	var allGood = true;

	for (var i=0;i<spriteList.length;i++)
		if (!spriteList[i].img.ready)
			allGood = false;

	if (allGood) {
		mode = "rollover";
		updateGrid(drawKin);
		updateTools(toolsKin);
	}
}


	// drawing utilities (image data)

// setPix()
function setPix(x,y,w,data,col) {
	// sets the rgba values in a pixelArray based on the x and y
	if (x*4+3+y*w*4 >= data.length) {
		throw "imageData error: attempting to draw outside the data array.";
	}
	data[x*4+y*w*4] = col.r;
	data[x*4+1+y*w*4] = col.g;
	data[x*4+2+y*w*4] = col.b;
	data[x*4+3+y*w*4] = col.a;
}

// drawBresLine()
function drawBresLine(x0,y0,x1,y1, w, data, col) {
	// draws a Bresenham line between two points into the given **image.data""
	// Note: this is not for drawing into a canvas, but for drawing aliased liens into a pixel array

	// Make sure we're using ints
	x0 = Math.round(x0);
	y0 = Math.round(y0);
	x1 = Math.round(x1);
	y1 = Math.round(y1);

	// Define differences and error check
	// (both directions simultaneously
	var dx = Math.abs(x1 - x0);
	var dy = Math.abs(y1 - y0);
	var sx = x0 < x1 ? 1 : -1;
	var sy = y0 < y1 ? 1 : -1;
	var err = (dx>dy ? dx : -dy)/2;

	while (true) {
		setPix(x0,y0,w,data, col);
		if (x0 === x1 && y0 === y1) break;
		var e2 = err;
		if (e2 > -dx) { err -= dy; x0 += sx; }
		if (e2 < dy) { err += dx; y0 += sy; }
	}
}

// drawVLine()
function drawVLine(x, y0, y1, w, data, col) {
	// Draws unantialiased vertical line into a image data array (for scanline filling)
	if (y0==y1) {
		setPix(x,y0,w,data,col);
	} else if (y0<y1) {
		for (var y = y0;y<=y1;y++) {
			setPix(x,y,w,data,col);
		}
	} else {
		for (var y = y1;y<=y0;y--) {
			setPix(x,y,w,data,col);
		}
	}
}

// drawHLine()
function drawHLine(x0, x1, y, w, data, col) {
	// Draws unantialiased horizontal line into a image data array
	if (x0==x1) {
		setPix(x0,y,w,data,col);
	} else if (x0<x1) {
		for (var x = x0;x<=x1;x++) {
			// console.log(x +","+ y);
			setPix(x,y,w,data,col);
		}
	} else {
		for (var x = x1;x<=x0;x--) {
			// console.log(x +","+ y);
			setPix(x,y,w,data,col);
		}
	}
}

function zsgn(a) {
	return (a<0) ? -1 : (a>0) ? 1 : 0;
}

// draw3dLine()
function draw3dLine(p0,p1,vGrid,idx) {
/**
 * translated from c source found here: ftp://ftp.isc.org/pub/usenet/comp.sources.unix/volume26/line3d
 *
 * which was...
 * "... dervied from DigitalLine.c published as 'Digital Line Drawing'
 * by Paul Heckbert from "Graphics Gems", Academic Press, 1990
 *
 * 3D modifications by Bob Pendleton. The original source code was in the public
 * domain, the author of the 3D version places his modifications in the
 * public domain as well."
 *
 * Generates points to make up a line between x0,y0,z0 and x1.y1.z1
*/
    
    var xd, yd, zd;
    var x, y, z;
    var ax, ay, az;
    var sx, sy, sz;
    var dx, dy, dz;
	var x0,y0,z0;
	var x1,y1,z1;
	

    x0 = p0.x;
    y0 = p0.y;
    z0 = p0.z;

	x1 = p1.x;
	y1 = p1.y;
	z1 = p1.z;

    dx = x1 - x0;
    dy = y1 - y0;
    dz = z1 - z0;
	
    ax = Math.abs(dx) << 1;
    ay = Math.abs(dy) << 1;
    az = Math.abs(dz) << 1;

    sx = zsgn(dx);
    sy = zsgn(dy);
    sz = zsgn(dz);

    x = x0;
    y = y0;
    z = z0;
	
	if (ax >= Math.max(ay,az))	{	// x dominant
        yd = ay - (ax >> 1);
        zd = az - (ax >> 1);
		for (;;) {
			// console.log("x dominant");
			vGrid.set(new Point3(x,y,z),idx);
            if (x == x1) {
                return;
            }

            if (yd >= 0) {
                y += sy;
                yd -= ax;
            }

            if (zd >= 0) {
                z += sz;
                zd -= ax;
            }

            x += sx;
            yd += ay;
            zd += az;
		}
	} else if (ay >= Math.max(ax,az)) {	// y dominant
        xd = ax - (ay >> 1);
        zd = az - (ay >> 1);
		for (;;) {
			// console.log("y dominant");
			vGrid.set(new Point3(x,y,z),idx);
            if (y == y1) {
                return;
            }

            if (xd >= 0) {
                x += sx;
                xd -= ay;
            }

            if (zd >= 0) {
                z += sz;
                zd -= ay;
            }

            y += sy;
            xd += ax;
            zd += az;
		}
	} else if (az >= Math.max(ax,ay)) {	// z dominant
        xd = ax - (az >> 1);
        yd = ay - (az >> 1);
		for (;;) {
			// console.log("z dominant");
			vGrid.set(new Point3(x,y,z),idx);
            if (z == z1) {
                return;
            }

            if (xd >= 0) {
                x += sx;
                xd -= az;
            }

            if (yd >= 0) {
                y += sy;
                yd -= az;
            }

            z += sz;
            xd += ax;
            yd += ay;
		}
	}
	//vGrid.set(p1,idx);

}

	// events

function thumbDown() {
	mode = "drag";
}
function drawDown() {
	mode = "firstdraw";
}
function globalUp() {
	mode = "rollover";
}
function noopHandler(evt) {
  evt.stopPropagation();
  evt.preventDefault();
}
function handleImageDrop(evt) {
	mode = "loading";
	evt.stopPropagation();
	evt.preventDefault();

	var files = evt.dataTransfer.files;
	var count = files.length;

	// Only works if 1 file is dropped
	if (count == 1) {

		var reader = new FileReader();
		reader.onloadend = handleReaderLoadEnd;
		reader.readAsDataURL(files[0]);
	}
}
function handleReaderLoadEnd(evt) {
	var img = new Image();
	img.onload = function() {
		parseImage(img);
	};
	img.src = evt.target.result;
	mode = "rollover";
	updateGrid(drawKin);
}
function handleZoom(val) {
	voxDim = val;
	resetGrid(drawKin);
	updateGrid(drawKin);
}
function handleKeyUp(evt) {
	//console.log("up: "+evt.keyCode);
	// shift: 16
	// ctrl: 17
	switch(evt.keyCode) {
		case 16:	// shift
			shiftDown = false;
			break;
		case 17: // ctrl
			ctrlDown = false; // not used
			break;
	}
}
function handleKeyDn(evt) {
	// console.log("dn: "+evt.keyCode);
	// todo: remap for qwerty keyboard
	
	switch (evt.keyCode) {
		case 16:	// shift
			shiftDown = true;
			break;
		case 17:	// ctrl
			ctrlDown = true;
			break;
			
		case 90:	// undo 'z'
			if (!undone) {
				voxGrid.swap(lastGrid);
				updateGrid(drawKin);
				undone = true;
			}
			break;
		case 89:	// redo 'y'
			if (undone) {
				voxGrid.swap(lastGrid);
				updateGrid(drawKin);
				undone = false;
			}
			break;
		case 88:	// cut 'x'
			lastGrid.replace(voxGrid);
			undone = false;
			drawGrid.copyDrawPlane(planeSwitch, planeVal);
			drawGrid.clearDrawPlane(planeSwitch, planeVal);
			updateGrid(drawKin);
			break;
		case 67:	// copy 'c'
			drawGrid.copyDrawPlane(planeSwitch, planeVal);
			updateGrid(drawKin);
			break;
		case 86:	// paste 'v'
			lastGrid.replace(voxGrid);
			undone = false;
			drawGrid.pasteDrawPlane(planeSwitch, planeVal);
			updateGrid(drawKin);
			break;

		case 38:	// up arrow
			if (planeSwitch == 0) {
				planeVal--;
				if (planeVal < 0) {
					planeVal = 0;
					planeSwitch = 2;
				}
			} else if (planeSwitch == 1) {
				planeVal--;
				if (planeVal < 0) {
					planeVal = 0;
				}
			} else {
				planeVal++;
				if (planeVal > drawGrid.zDim-1) {
					planeSwitch = 1;
					planeVal = drawGrid.yDim-1;
				}
			}
			fullRedraw = true;
			updateGrid(drawKin);
			break;
		case 40:	// dn arrow
			if (planeSwitch == 0) {
				planeVal++;
				if (planeVal > drawGrid.xDim-1) {
					planeVal = drawGrid.xDim-1;
				}
			} else if (planeSwitch == 1) {
				planeVal++;
				if (planeVal > drawGrid.yDim-1) {
					planeSwitch = 2;
					planeVal = drawGrid.zDim-1;
				}
			} else {
				planeVal--;
				if (planeVal < 0) {
					planeSwitch = 0;
					planeVal = 0;
				}
			}
			fullRedraw = true;
			updateGrid(drawKin);
			break;

		case 189: // x up
			drawGrid.x++;
			constrainDrawGrid();
			updateGrid(drawKin);
			break;
		case 82: // x dn
			drawGrid.x--;
			constrainDrawGrid();
			updateGrid(drawKin);
			break;
		case 78: // y up
			drawGrid.y++;
			constrainDrawGrid();
			updateGrid(drawKin);
			break;
		case 191: // y dn
			drawGrid.y--;
			constrainDrawGrid();
			updateGrid(drawKin);
			break;
		case 76: // z up
			drawGrid.z++;
			constrainDrawGrid();
			updateGrid(drawKin);
			break;
		case 83: // z dn
			drawGrid.z--;
			constrainDrawGrid();
			updateGrid(drawKin);
			break;
	}
}


// setRegions()
function setRegions(kin) {
	var context = kin.getContext();
    var thumbPt;
    var planePt;
	kin.beginRegion();
	context.beginPath();
	context.rect(0,0,centerPt.x*2,centerPt.y*2);
	context.closePath();
	kin.addRegionEventListener("mouseup", globalUp);
	kin.closeRegion();

	if (planeSwitch == 0) {	// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

		// drag thumb
		thumbPt = voxPointToCanvas(new Point3(planeVal,drawGrid.yDim,0));

		kin.beginRegion();

		context.beginPath();
		context.moveTo(thumbPt.x, thumbPt.y);

		context.lineTo(thumbPt.x+11, thumbPt.y+5);
		context.lineTo(thumbPt.x+11, thumbPt.y+16);
		context.lineTo(thumbPt.x-11, thumbPt.y+5);
		context.lineTo(thumbPt.x-11, thumbPt.y-6);
		context.closePath();

		kin.addRegionEventListener("mousedown", thumbDown);
		kin.closeRegion();

		// draw plane
		planePt = voxPointToCanvas(new Point3(planeVal,0,0));

		kin.beginRegion();
		context.beginPath();
		context.moveTo(planePt.x,planePt.y);
		context.lineTo(planePt.x,planePt.y-centerPt.y);
		context.lineTo(planePt.x-centerPt.x,planePt.y-centerPt.y/2);
		context.lineTo(planePt.x-centerPt.x,planePt.y+centerPt.y/2);
		context.closePath();

		kin.addRegionEventListener("mousedown", drawDown);
		kin.closeRegion();

	} else if (planeSwitch == 1) {	// yyyyyyyyyyyyyyyyyyyyyyyyyy

		// drag thumb
		thumbPt = voxPointToCanvas(new Point3(0,planeVal,drawGrid.zDim));

		kin.beginRegion();

		context.beginPath();
		context.moveTo(thumbPt.x, thumbPt.y);

		context.lineTo(thumbPt.x+11,thumbPt.y-5)
		context.lineTo(thumbPt.x,thumbPt.y-12);
		context.lineTo(thumbPt.x-23,thumbPt.y);
		context.lineTo(thumbPt.x-11,thumbPt.y+5);

		context.closePath();

		kin.addRegionEventListener("mousedown", thumbDown);

		kin.closeRegion();

		// draw plane
		planePt = voxPointToCanvas(new Point3(0,planeVal,0));

		kin.beginRegion();
		context.beginPath();
		context.moveTo(planePt.x,planePt.y);
		context.lineTo(planePt.x,planePt.y-centerPt.y);
		context.lineTo(planePt.x+centerPt.x,planePt.y-centerPt.y/2);
		context.lineTo(planePt.x+centerPt.x,planePt.y+centerPt.y/2);
		context.closePath();
		kin.addRegionEventListener("mousedown", drawDown);
		kin.closeRegion();

	} else {	// zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz

		// drag thumb
		thumbPt = voxPointToCanvas(new Point3(0,drawGrid.yDim,planeVal));

		kin.beginRegion();

		context.beginPath();
		context.moveTo(thumbPt.x, thumbPt.y);

		context.lineTo(thumbPt.x,thumbPt.y+11);
		context.lineTo(thumbPt.x-11,thumbPt.y+5);
		context.lineTo(thumbPt.x-11,thumbPt.y-17);
		context.lineTo(thumbPt.x,thumbPt.y-11);

		context.closePath();

		kin.addRegionEventListener("mousedown", thumbDown);

		kin.closeRegion();

		// draw plane
		planePt = voxPointToCanvas(new Point3(0,0,planeVal));

		kin.beginRegion();
		context.beginPath();
		context.moveTo(planePt.x,planePt.y);
		context.lineTo(0,planePt.y+centerPt.y/2);
		context.lineTo(planePt.x,planePt.y+centerPt.y);
		context.lineTo(centerPt.x*2,planePt.y+centerPt.y/2);
		context.closePath();
		kin.addRegionEventListener("mousedown", drawDown);
		kin.closeRegion();
	}
}

// Voxes

// getVoxIdx
function getVoxIdx(col) {
	for (var i=0;i<voxes.length;i++) {
		if (voxes[i].matches(col))
			return i;
	}
	return false;
}
// createVox
function createVoxImage(col, dim, type) {
	// draw the image for the vox
	// types:
	//	- "filled": a regular colored vox
	//	- "empty": just the lines
	//	- "cursor": the cursor vox

	if (col.a == 0)
		return null;

	var voxW = dim*2+1, voxH = dim*2+1;

	// create an empty canvas for the vox
	var voxCanvas = makeCanvas(voxW,voxH);
	var voxCtx = voxCanvas.getContext("2d");
	var voxImgData = voxCtx.createImageData(voxW,voxH);
	var voxData = voxImgData.data;

		// lines
	var lc1,lc2,lc3,lc4;
	// line colors
	if (type == "empty") {
		lc1 = new Color(255,255,255,255);
		lc2 = new Color(255,255,255,255);
		lc3 = new Color(255,255,255,255);
	} else {
		lc1 = col.getDark(0.65);
		lc2 = col.getDark(0.45);
		lc3 = col.getDark(0.35);
	}

	// draw the lines
	drawBresLine(1,dim/2-1,dim,0,voxW,voxData,lc1);
	drawBresLine(dim-1,0,dim*2,dim/2,voxW,voxData,lc1);

	drawBresLine(0,dim*0.5,0,dim*1.5,voxW,voxData,lc2);
	drawBresLine(1,dim*0.5+1,dim,dim,voxW,voxData,lc2);
	drawBresLine(1,dim*1.5+1,dim,dim*2,voxW,voxData,lc2);

	drawBresLine(dim,dim,dim,dim*2,voxW,voxData,lc3);
	drawBresLine(dim*2,dim*0.5,dim*2,dim*1.5,voxW,voxData,lc3);
	drawBresLine(dim+1,dim-1,dim*2,dim*0.5,voxW,voxData,lc3);
	drawBresLine(dim+1,dim*2-1,dim*2,dim*1.5,voxW,voxData,lc3);

	if (type == "filled") {

		// fill x - use vertical lines (vl)
		for (var vl = dim+1;vl<dim*2;vl++)
			drawVLine(vl,dim-Math.ceil((vl-dim)/2)+1,dim*2-Math.ceil((vl-dim)/2)-1,voxW,voxData,col.getDark(0.6));

		// fill y - use vertical lines (vl)
		for (var vl = 1;vl<dim;vl++)
			drawVLine(vl,dim+Math.ceil((vl-dim)/2)+1,dim*2+Math.ceil((vl-dim)/2)-1,voxW,voxData,col.getDark(0.8));

		// fill z - use horizontal lines (hl)
		for (var hl = 1;hl<dim/2;hl++)
			drawHLine(dim-hl*2+1,dim+hl*2-2,hl,voxW,voxData,col);

		drawHLine(1,dim*2-2,dim/2,voxW,voxData,col);

		for (var hl = dim/2+1;hl<dim;hl++)
			drawHLine(dim-(dim-hl)*2+1,dim+(dim-hl)*2-2,hl,voxW,voxData,col);

	} else if (type == "cursor") {

		var white = new Color(255,255,255,255);
		var black = new Color(0,0,0,255);

		// p1  p2
		// p4  p3
		var px1,px2,px3,px4,py1,py2,py3,py4,pz1,pz2,pz3,pz4;

		var gOut, gIn, sGut,bGut;

		switch (dim) {
			case 4:
				gOut = 1;
				gIn=0;
				break;
			case 6:
			case 8:
				gOut = 2;
				gIn = 0;
				break;
			case 10:
			case 14:
				gOut = 2;
				gIn = 4;
				break;
			case 16:
			case 18:
				gOut = 3;
				gIn = 5;
				break;
			default:
				gOut = Math.round(dim/5);
				gIn = gOut*2;
		}

		// out (white)
		bGut = gOut;
		sGut = Math.floor(bGut/2);

		px1 = new Point2(dim +		bGut,	dim		+bGut 	-sGut);
		px2 = new Point2(dim*2 - 	bGut, 	dim*0.5 +bGut 	+sGut);
		px3 = new Point2(dim*2 - 	bGut,	dim*1.5	-bGut	+sGut);
		px4 = new Point2(dim +		bGut,	dim*2	-bGut	-sGut);
		drawBresLine(px1.x+1,px1.y-1,px2.x,px2.y,voxW,voxData,white);
		drawBresLine(px4.x+1,px4.y-1,px3.x,px3.y,voxW,voxData,white);
		drawVLine(px1.x,px1.y,px4.y,voxW,voxData,white);
		drawVLine(px2.x,px2.y,px3.y,voxW,voxData,white);

		py1 = new Point2(	   		bGut,	dim*0.5	+bGut	+sGut	);
		py2 = new Point2(dim -	bGut,	dim		+bGut	-sGut	);
		py3 = new Point2(dim -	bGut,	dim*2	-bGut	-sGut	);
		py4 = new Point2(	   		bGut,	dim*1.5	-bGut	+sGut	);
		drawBresLine(py1.x+1,py1.y+1,py2.x,py2.y,voxW,voxData,white);
		drawBresLine(py4.x+1,py4.y+1,py3.x,py3.y,voxW,voxData,white);
		drawVLine(py1.x,py1.y,py4.y,voxW,voxData,white);
		drawVLine(py2.x,py2.y,py3.y,voxW,voxData,white);

		pz1 = new Point2(dim,bGut);
		pz2 = new Point2(dim*2-bGut*2,dim*0.5);
		pz3 = new Point2(dim,dim-bGut);
		pz4 = new Point2(bGut*2-1,dim*0.5);
		drawBresLine(pz1.x-1,pz1.y,pz2.x,pz2.y,voxW,voxData,white);
		drawBresLine(pz2.x,pz2.y,pz3.x-1,pz3.y,voxW,voxData,white);
		drawBresLine(pz3.x,pz3.y,pz4.x,pz4.y,voxW,voxData,white);
		drawBresLine(pz4.x,pz4.y,pz1.x,pz1.y,voxW,voxData,white);

		// in (black)
		bGut = gIn
		sGut = Math.floor(bGut/2);
		//console.log(dim + ": black big:" + bGut + " black small:" + sGut);

		if (bGut>0) {
			px1 = new Point2(dim +		bGut,	dim		+bGut 	-sGut);
			px2 = new Point2(dim*2 - 	bGut, 	dim*0.5 +bGut 	+sGut);
			px3 = new Point2(dim*2 - 	bGut,	dim*1.5	-bGut	+sGut);
			px4 = new Point2(dim +		bGut,	dim*2	-bGut	-sGut);
			drawBresLine(px1.x+1,px1.y-1,px2.x,px2.y,voxW,voxData,black);
			drawBresLine(px4.x+1,px4.y-1,px3.x,px3.y,voxW,voxData,black);
			drawVLine(px1.x,px1.y,px4.y,voxW,voxData,black);
			drawVLine(px2.x,px2.y,px3.y,voxW,voxData,black);

			py1 = new Point2(	   	bGut,	dim*0.5	+bGut	+sGut	);
			py2 = new Point2(dim-	bGut,	dim		+bGut	-sGut	);
			py3 = new Point2(dim- bGut,	dim*2	-bGut	-sGut	);
			py4 = new Point2(	   	bGut,	dim*1.5	-bGut	+sGut	);
			drawBresLine(py1.x+1,py1.y+1,py2.x,py2.y,voxW,voxData,black);
			drawBresLine(py4.x+1,py4.y+1,py3.x,py3.y,voxW,voxData,black);
			drawVLine(py1.x,py1.y,py4.y,voxW,voxData,black);
			drawVLine(py2.x,py2.y,py3.y,voxW,voxData,black);

			pz1 = new Point2(dim,bGut);
			pz2 = new Point2(dim*2-bGut*2,dim*0.5);
			pz3 = new Point2(dim,dim-bGut);
			pz4 = new Point2(bGut*2-1,dim*0.5);
			drawBresLine(pz1.x-1,pz1.y,pz2.x,pz2.y,voxW,voxData,black);
			drawBresLine(pz2.x,pz2.y,pz3.x-1,pz3.y,voxW,voxData,black);
			drawBresLine(pz3.x,pz3.y,pz4.x,pz4.y,voxW,voxData,black);
			drawBresLine(pz4.x,pz4.y,pz1.x,pz1.y,voxW,voxData,black);
		}
	}
	// put the ImageData into the context
	voxCtx.putImageData(voxImgData, 0,0);

	return voxCanvas;
}
// Vox()
/**
 * Vox
 * A voxel with a given image and rgb color.
 * @constructor
 */
function Vox(color) {
	this.color = color;
	this.img = createVoxImage(color,voxDim,"filled");
	this.toolImg = createVoxImage(color,10,"filled"); // todo: move this out so it's not rebuild everytime we zoom
	this.matches = function (col) {
		if (col.r == this.color.r && col.g == this.color.g && col.b == this.color.b && col.a == this.color.a)
			return true;
		return false;
	};
}
// initVoxImages()
function initVoxes() {

	voxes[0] = new Vox(new Color(0,0,0,0));
	voxes[1] = new Vox(new Color(255,0,0,255));
	voxes[2]  = new Vox(new Color(255,127,0,255));
	voxes[3]  = new Vox(new Color(255,255,0,255));
	voxes[4]  = new Vox(new Color(127,255,0,255));
	voxes[5]  = new Vox(new Color(0,255,0,255));
	voxes[6]  = new Vox(new Color(0,255,127,255));
	voxes[7]  = new Vox(new Color(0,255,255,255));
	voxes[8]  = new Vox(new Color(0,127,255,255));
	voxes[9]  = new Vox(new Color(0,0,255,255));
	voxes[10] = new Vox(new Color(127,0,255,255));
	voxes[11] = new Vox(new Color(255,0,255,255));
	voxes[12] = new Vox(new Color(255,0,127,255));
	voxes[13] = new Vox(new Color(255,255,255,255));
	voxes[14] = new Vox(new Color(127,127,127,255));
	voxes[15] = new Vox(new Color(51,51,51,255));
}

// grid data i/o
function drawData() {
	// data png:
	//	0:		dim of full cube (hardcoded to 32,32,32 for now)
	//	1:		dim of clipped cube (same as full cube for now)
	//	2:		pos of clipped cube in full cube (0,0,0 for now)
	//	3:		dim of tiles (1,32 for now)
	//	4:		number of indexes
	//	5-...:	indexes

	// create an empty off screen (os) canvas
	var iData = new IData(voxGrid.xDim,voxGrid.yDim*voxGrid.zDim+1);

	// full grid dims
	iData.data[0] = voxGrid.xDim;
	iData.data[1] = voxGrid.yDim;
	iData.data[2] = voxGrid.zDim;
	iData.data[3] = 255;

	// clipped dims
	iData.data[4] = voxGrid.xDim;
	iData.data[5] = voxGrid.yDim;
	iData.data[6] = voxGrid.zDim;
	iData.data[7] = 255

	// position of clipped cube
	iData.data[8] = 0;
	iData.data[9] = 0;
	iData.data[10] = 0;
	iData.data[11] = 255

	// tile dims
	iData.data[12] = 1;
	iData.data[13] = voxGrid.zDim;
	//data[14] blank
	iData.data[15] = 255;

	// number of indexes
	iData.data[16] = voxes.length;
	iData.data[19] = 256;

	// write the indexed voxes
	for (var idx = 0;idx<voxes.length;idx++) {
		iData.data[20+idx*4] = voxes[idx].color.r;
		iData.data[20+idx*4+1] = voxes[idx].color.g;
		iData.data[20+idx*4+2] = voxes[idx].color.b
		iData.data[20+idx*4+3] = voxes[idx].color.a;
	}

	// write the vox grid
	var i = voxGrid.xDim*4;
	for (var z = 0;z<voxGrid.zDim;z++) {
		for (var y = 0;y<voxGrid.yDim;y++) {
			for (var x = 0;x<voxGrid.xDim;x++) {
				var vox = voxes[voxGrid.get(new Point3(x,y,z))]
				iData.data[i] 		= vox.color.r;
				iData.data[i+1] 	= vox.color.g;
				iData.data[i+2] 	= vox.color.b;
				iData.data[i+3]	= vox.color.a;
				i+=4;
			}
		}
	}

	// put the ImageData into the context
	iData.ctx.putImageData(iData.imgData, 0,0);

	return iData.can;
}
function parseImage(img) {

	// create an empty canvas
	var iData = new IData(img.width,img.height);

	// draw the image
	iData.can.getContext("2d").drawImage(img,0,0);
	// get the data
	var data = iData.can.getContext("2d").getImageData(0,0,img.width,img.height).data;

	// init grid and voxes
	voxGrid = new VoxGrid(data[0],data[1],data[2],data[16]);
	voxes = [];

	// generate the vox indexes
	for (var i=0;i<data[16];i++) {
		var c = new Color(data[20+i*4],data[20+i*4+1],data[20+i*4+2],data[20+i*4+3]);
		voxes[i] = new Vox(c);
	}

	// read the vox grid
	var i = voxGrid.xDim*4;
	for (var z = 0;z<voxGrid.zDim;z++) {
		for (var y = 0;y<voxGrid.yDim;y++) {
			for (var x = 0;x<voxGrid.xDim;x++) {
				var c = new Color(data[i],data[i+1],data[i+2],data[i+3]);
				var idx = getVoxIdx(c);
				if (idx)
					voxGrid.set(new Point3(x,y,z),idx);
				else
					voxGrid.set(new Point3(x,y,z),0);
				i+=4;
			}
		}
	}
}

// updateGrid()
function updateGrid(kin) {
	var context = kin.getContext();

	if (mode == "loading") {
		context.font = "30pt Calibri";
		context.textAlign = "center";
		context.fillText("Loading...", centerPt.x, centerPt.y);
		return;
	}

	var mousePos = kin.getMousePos();

	// set the planeSwitch, planeVal if dragging
	if (mode == "drag") {
		// split the screen up into three sections
		var y1 =  voxPointToCanvas(new Point3(0,drawGrid.yDim,drawGrid.zDim)).y;
		var y2 = voxPointToCanvas(new Point3(0,drawGrid.yDim,0)).y;
		var y3 = voxPointToCanvas(new Point3(drawGrid.xDim,drawGrid.yDim,0)).y;
		if (mousePos.y > y2 && mousePos.y < y3) {				// xxxxxxxxxxxxxxx
			planeSwitch = 0;
			planeVal = Math.round( (mousePos.y-y2) / ( (y3-y2) / (drawGrid.zDim-1)));
		} else if (mousePos.y < y1) {										// yyyyyyyyyyyyyyy
			planeSwitch = 1;
			planeVal = Math.round( (mousePos.y) / ( (y1) / (drawGrid.zDim-1)));
		} else if (mousePos.y >= y1 && mousePos.y <= y2) {	// zzzzzzzzzzzzzzz
			planeSwitch = 2;
			planeVal = Math.round( (y2-mousePos.y) / ( (y2-y1) / (drawGrid.zDim-1)));
		}
		fullRedraw = true;
	}
	
	// set the regions for events (should init this and then move it into "drag" above)
	setRegions(kin);

	if (mode == "firstdraw") {
		// console.log(shiftDown);
		lastGrid.replace(voxGrid);
		var dPt = mouseToVoxPoint(mousePos,planeVal);
		var vPt = new Point3(dPt.x+drawGrid.x,dPt.y+drawGrid.y,dPt.z+drawGrid.z);
		if ( isOnDrawPlane(dPt) ) {
			voxGrid.set(vPt,drawIdx);
			if (shiftDown) {
				// draw line from last lastVoxPt
				if (lastVoxPt)
					draw3dLine(lastVoxPt,vPt,voxGrid,drawIdx);
				fullRedraw = true;
			}
			lastVoxPt = vPt;
		}

		undone = false;
		mode = "draw";
	} else if (mode == "draw") {
		// if we're drawing, add vox to voxGrid
		
		// add the new vox
		var dPt = mouseToVoxPoint(mousePos,planeVal);
		var vPt = new Point3(dPt.x+drawGrid.x,dPt.y+drawGrid.y,dPt.z+drawGrid.z);
		if ( isOnDrawPlane(dPt) ) {
			voxGrid.set(vPt,drawIdx);
			lastVoxPt = vPt;
		}
	}

	drawGridAll(kin.getCanvas(),mousePos);
}

// drawGridFull
function drawGridFull(canvas,mousePos) {
	fullRedraw = true;
	drawGridAll(canvas,mousePos);
}

// drawGrid
function drawGridAll(canvas,mousePos) {
	var context = canvas.getContext("2d");
	var w = canvas.width, h = canvas.height;
	
	
	// handle the cursor
	var cursorVoxPt = null;
	var cursorCanvasPt = null;

	if (mousePos!=null) {
		cursorVoxPt = mouseToVoxPoint(mousePos,planeVal); // in drawGrid space

		if ( isOnDrawPlane(cursorVoxPt) ) {
			// this vox is on the draw plane
			cursorCanvasPt = voxPointToCanvas(cursorVoxPt);
			// kill real cursor
			canvas.style.cursor = "none";
		} else {
			// restore real cursor
			canvas.style.cursor = "default";
		}
	}

	// handle thumb and draw plane
	var thumbPt,thumbImg, thumbOffset = {x:0,y:0};
	var planeImg, planePos = {x:0,y:0};
	
	if (planeSwitch == 0) {				// x plane
		thumbPt = voxPointToCanvas(new Point3(planeVal,drawGrid.yDim,0));
		thumbImg = xThumb.img;
		thumbOffset = xThumb.offset;
		planeImg = xPlaneImg;
		planePos.x = thumbPt.x-1,
		planePos.y = thumbPt.y-voxDim*drawGrid.zDim-voxDim*drawGrid.yDim/2-1;
	} else if (planeSwitch == 1) {		// y plane
		thumbPt = voxPointToCanvas(new Point3(0,planeVal,drawGrid.zDim));
		thumbImg = yThumb.img;
		thumbOffset = yThumb.offset;
		planeImg = yPlaneImg;
		planePos.x = thumbPt.x-1;
		planePos.y = thumbPt.y-1;
	} else { 										// z plane
		thumbPt = voxPointToCanvas(new Point3(0,drawGrid.yDim,planeVal));
		thumbImg = zThumb.img;
		thumbOffset = zThumb.offset;
		planeImg = zPlaneImg;
		planePos.x = thumbPt.x-1;
		planePos.y = thumbPt.y-voxDim*(drawGrid.xDim+drawGrid.yDim)/4-1;
	}
	
	// voxes
	var ctxBehind = voxesBehind.getContext("2d");
	var ctxInfront = voxesInfront.getContext("2d");
	var ctxCurr = voxesCurr.getContext("2d");
	
	// clear
	if (fullRedraw) {
		ctxBehind.clearRect(0,0,w,h);
		ctxInfront.clearRect(0,0,w,h);
	}
	ctxCurr.clearRect(0,0,w,h);
	
	// rebuild
	for (var x = 0; x<drawGrid.xDim;x++) {
		for (var y = 0;y<drawGrid.yDim;y++) {
			for (var z = 0;z<drawGrid.zDim;z++) {
				var dPt = new Point3(x,y,z);														// the point in the draw cube
				var vPt = new Point3(drawGrid.x+x,drawGrid.y+y,drawGrid.z+z);	// the point translated into the full vox cube
				var idx = voxGrid.get(vPt);

				// vox
				if (idx > 0) {
					var voxImg = voxes[voxGrid.get(vPt)].img;
					var cPt = voxPointToCanvas(dPt);
					
					var planeTest;
					if (planeSwitch == 0)
						planeTest = x;
					else if (planeSwitch == 1)
						planeTest = y;
					else
						planeTest = z;
					
					if (planeTest<planeVal) {
						if (fullRedraw) {
							ctxBehind.drawImage(voxImg,cPt.x+voxOffset.x,cPt.y+voxOffset.y);
						}
					} else if (planeTest>planeVal) {
						if (fullRedraw) {
							ctxInfront.drawImage(voxImg,cPt.x+voxOffset.x,cPt.y+voxOffset.y);
						}
					} else {
						ctxCurr.drawImage(voxImg,cPt.x+voxOffset.x,cPt.y+voxOffset.y);
					}
				} // idx
				if (cursorCanvasPt != null && x == cursorVoxPt.x && y == cursorVoxPt.y && z == cursorVoxPt.z)
					ctxCurr.drawImage(cursor, cursorCanvasPt.x+voxOffset.x, cursorCanvasPt.y+voxOffset.y);
			} // z
		} // y
	} // x

	fullRedraw = false;


	
	// draw all the images to the canvas

	context.clearRect(0, 0, w, h);
	
	// background
	if (mode != "snapnogrid")
		context.drawImage(gndImg, 0, 0);

	context.drawImage(voxesBehind, 0, 0);

	if (mode != "snap" && mode!= "snapnogrid") {
		context.drawImage(thumbImg,thumbPt.x+thumbOffset.x,thumbPt.y+thumbOffset.y);
		context.drawImage(planeImg,planePos.x,planePos.y);
	}
	
	context.drawImage(voxesCurr, 0, 0);
	context.drawImage(voxesInfront, 0, 0);
}



// updateTools
function updateTools(kin) {
	if (mode == "loading")
		return;
	var context = kin.getContext();
	var ctx = kin.getContext();
	var w = kin.getCanvas().width;
	var h = kin.getCanvas().height;

	kin.clear();
	ctx.drawImage(toolsGnd.img,0,0);

	// draw vox pickers
	var voxStep = 20;
	var voxX = 10;
	var voxY = 10;

	for (var i = 0;i<voxes.length;i++) {
		var y = voxY+voxStep*i;

		if (voxes[i].img == null)
			ctx.drawImage(emptyVox,voxX,y);
		else
			ctx.drawImage(voxes[i].toolImg,voxX,y);

		if (i==drawIdx)
			ctx.drawImage(toolCursor,voxX,y);
	}

	// mouse events for voxes
	kin.beginRegion();

	ctx.beginPath();
	ctx.rect(voxX,voxY,21,voxes.length*voxStep+1);
	ctx.closePath();

	kin.addRegionEventListener("mousedown", function(){
		drawIdx = Math.floor((kin.getMousePos().y-10)/voxStep);
	});

	kin.closeRegion();

	// actions

	var actX=40, actY=30, actStep=20;

	ctx.drawImage(actionsLow.img,actX,actY);

	kin.beginRegion();

	ctx.beginPath();
	ctx.rect(actX,actY+10,31,totalActions*actStep);
	ctx.closePath();

	kin.addRegionEventListener("mousedown", function() {
		var actNum = Math.floor((kin.getMousePos().y-actY-10)/actStep);
		// console.log(actNum);

		switch (actNum) {
			case 0:	// undo
				if (!undone) {
					voxGrid.swap(lastGrid);
					updateGrid(drawKin);
					undone = true;
				}
				break;
			case 1:	// redo
				if (undone) {
					voxGrid.swap(lastGrid);
					updateGrid(drawKin);
					undone = false;
				}
				break;
			case 2:	// cut
				lastGrid.replace(voxGrid);
				undone = false;
				drawGrid.copyDrawPlane(planeSwitch, planeVal);
				drawGrid.clearDrawPlane(planeSwitch, planeVal);
				updateGrid(drawKin);
				break;
			case 3:	// copy
				drawGrid.copyDrawPlane(planeSwitch, planeVal);
				updateGrid(drawKin);
				break;
			case 4:	// paste
				lastGrid.replace(voxGrid);
				undone = false;
				drawGrid.pasteDrawPlane(planeSwitch, planeVal);
				updateGrid(drawKin);
				break;
			case 5:	// snap grid
				// create an empty off screen (os) canvas
				var osCanvas = makeCanvas(centerPt.x*2,centerPt.y*2);

				var oldMode = mode;
				mode = "snap";
				drawGridFull(osCanvas,null);
				mode = oldMode;

				// pop into new window
				window.open(osCanvas.toDataURL('image/png'),"voxdraw"+windowId,"width=" + osCanvas.width + ",height=" + osCanvas.height + ",status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=0");
				windowId++;
				break;
			case 6:	// snap no grid
				// create an empty off screen (os) canvas
				var osCanvas = makeCanvas(centerPt.x*2,centerPt.y*2);

				var oldMode = mode;
				mode = "snapnogrid";
				drawGridFull(osCanvas,null);
				mode = oldMode;

				// pop into new window
				window.open(osCanvas.toDataURL('image/png'),"voxdraw"+windowId,"width=" + osCanvas.width + ",height=" + osCanvas.height + ",status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=0");
				windowId++;
				break;
			case 7:	// save
				var osCanvas = drawData();

				// pop into new window
				window.open(osCanvas.toDataURL('image/png'),"voxdraw"+windowId,"width=" + osCanvas.width + ",height=" + osCanvas.height + ",status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=0");
				windowId++;
				break;
		}
	});


	kin.closeRegion();
}

// constrainDrawGrid()
function constrainDrawGrid() {
	// make sure the draw grid doesn't expend outside the voxGrid
	if (drawGrid.x < 0)
		drawGrid.x = 0;
	else if (drawGrid.x > voxGrid.xDim-drawGrid.xDim)
		drawGrid.x = voxGrid.xDim-drawGrid.xDim;
	if (drawGrid.y < 0)
		drawGrid.y = 0;
	else if (drawGrid.y > voxGrid.yDim-drawGrid.yDim)
		drawGrid.y = voxGrid.yDim-drawGrid.yDim;
	if (drawGrid.z < 0)
		drawGrid.z = 0;
	else if (drawGrid.z > voxGrid.zDim-drawGrid.zDim)
		drawGrid.z = voxGrid.zDim-drawGrid.zDim;

	// make sure the planeVal is inside the draw grid
	switch (planeSwitch) {
		case 0:	// x
			if (planeVal>drawGrid.xDim-1)
				planeVal=drawGrid.xDim-1;
			break;
		case 1:	// y
			if (planeVal>drawGrid.yDim-1)
				planeVal=drawGrid.yDim-1;
			break;
		case 2:	// z
			if (planeVal>drawGrid.zDim-1)
				planeVal=drawGrid.zDim-1;
			break;
	}
}

function makeCanvas(w,h) {
	var can = document.createElement('canvas');
	can.width = w;
	can.height = h;
	return can;
}

// createXtile()
function createXtile(w,h,fillCol, loCol,hiCol ) {
	var iData = new IData(w+1,h+1);

	// fill
	if (fillCol.a>0) {
		// don't fill if the fill color is transparent
		for (var y = 0;y<h/3;y++)
			drawHLine(w-y*2-1,w,y,  w+1,iData.data,fillCol);
		for (var y = h/3;y<h*2/3;y++)
			drawHLine(0,w,y,  w+1,iData.data,fillCol);
		for (var y = h*2/3;y<h;y++)
			drawHLine(0,w-(y-h*2/3)*2,y,  w+1,iData.data,fillCol);
	}

	// light lines
	for (var i = 1;i<4;i++) {
		drawVLine(voxDim*i,h/3-voxDim/2*i,h-voxDim/2*i,   w+1,iData.data,loCol);
		drawBresLine(1,h/3+voxDim*i-1,w, voxDim*i,   w+1,iData.data,loCol);
	}

	// dark lines
	drawVLine(0,h/3,h,   w+1,iData.data,hiCol);
	drawVLine(w,0,h*2/3,   w+1,iData.data,hiCol);
	drawBresLine(1,h/3-1,w,0,   w+1,iData.data,hiCol);
	drawBresLine(1,h-1,w,h*2/3,   w+1,iData.data,hiCol);

	// draw
	iData.ctx.putImageData(iData.imgData,0,0);
	return iData.can;
}
// createYtile()
function createYtile(w,h,fillCol, loCol,hiCol ) {
	var iData = new IData(w+1,h+1);

	// fill
	if (fillCol.a>0) {
		for (var y = 0;y<h/3;y++) {
			drawHLine(0,y*2,y,  w+1,iData.data,fillCol);
		}
		for (var y = h/3;y<h*2/3;y++) {
			drawHLine(0,w,y,  w+1,iData.data,fillCol);
		}
		drawHLine(0,w,y,  w+1,iData.data,fillCol);
		for (var y = h*2/3;y<h;y++) {
			drawHLine((y-h*2/3)*2+1,w,y+1,  w+1,iData.data,fillCol);
		}
	}

	// light lines
	for (var i = 1;i<4;i++) {
		drawVLine(voxDim*i,voxDim/2*i,h-voxDim/2*(4-i),   w+1,iData.data,loCol);
		drawBresLine(1,voxDim*i+1,w,h/3+voxDim*i,   w+1,iData.data,loCol);
	}

	// dark lines
	drawVLine(w,h/3,h,   					w+1,iData.data,hiCol);
	drawVLine(0,0,h*2/3,   				w+1,iData.data,hiCol);
	drawBresLine(1,1,w,h/3,   			w+1,iData.data,hiCol);
	drawBresLine(1,h*2/3+1,w,h,  	w+1,iData.data,hiCol);

	// draw
	iData.ctx.putImageData(iData.imgData,0,0);
	return iData.can;
}
// createZtile()
function createZtile(w,h,fillCol, loCol,hiCol ) {
	var iData = new IData(w+1,h+1);

	// fill
	if (fillCol.a>0) {
		for (var y = 0;y<h/2;y++) {
			drawHLine(w/2-y*2-1,w/2+y*2,y,	w+1,iData.data,fillCol);
			drawHLine(w/2-y*2-1,w/2+y*2,h-y,	w+1,iData.data,fillCol);
		}
		drawHLine(w/2-h,w/2+h,h/2,	w+1,iData.data,fillCol);
	}
	// light lines
	for (var i = 1;i<4;i++) {
		var step = voxDim/2;
		drawBresLine(i*2*step-1, h/2+i*step, w/2+i*2*step, i*step,	w+1,iData.data,loCol);
		drawBresLine(i*2*step-1, h/2-i*step,  w/2+i*2*step, h-i*step,	w+1,iData.data,loCol);
	}

	// dark lines
	setPix(0,h/2,	w+1,iData.data,hiCol);
	drawBresLine(1,h/2-1,w/2,0,	w+1,iData.data,hiCol);
	drawBresLine(w/2-1,0,w,h/2,	w+1,iData.data,hiCol);
	drawBresLine(w/2-1,h,w,h/2,	w+1,iData.data,hiCol);
	drawBresLine(1,h/2+1,w/2,h,	w+1,iData.data,hiCol);

	// draw
	iData.ctx.putImageData(iData.imgData, 0,0);
	return iData.can;
}


// createGnd()
function createGnd() {
	// creates the background image and draw planes

	// colors
	var gndFillCol = new Color(204,204,204,255),
		gndLoCol = new Color(178,178,178,255),
		gndHiCol = new Color(153,153,153,255);
	var planeFillCol = new Color(0,0,0,0),
		planeHiCol = new Color(255,255,255,255),
		planeLoCol = new Color(225,225,225,255);

	var gndW = voxDim*(drawGrid.xDim+drawGrid.yDim)+voxDim*21;
	var gndH = gndW;
	var cx = centerPt.x-1, cy=centerPt.y-1; // center of the canvas for drawing

	// create empty canvases for the ground and draw planes
	voxesBehind = makeCanvas(gndW, gndH);
	voxesInfront = makeCanvas(gndW, gndH);
	voxesCurr = makeCanvas(gndW, gndH);

	var gndCan = makeCanvas(gndW,gndH);
	var gndCtx = gndCan.getContext("2d");

	// create the tiles
	// a tile is 4x4 voxes
	var xTileW = 4*voxDim;
	var xTileH = 4*voxDim*1.5;
	var xTile = createXtile(xTileW,xTileH,gndFillCol,gndLoCol,gndHiCol);
	var xPlaneTile = createXtile(xTileW,xTileH,planeFillCol,planeLoCol,planeHiCol);

	var yTileW = 4*voxDim;
	var yTileH = 4*voxDim*1.5;
	var yTile = createYtile(yTileW,yTileH,gndFillCol,gndLoCol,gndHiCol);
	var yPlaneTile = createYtile(yTileW,yTileH,planeFillCol,planeLoCol,planeHiCol);

	var zTileW = 4*voxDim*2;
	var zTileH = 4*voxDim;
	var zTile = createZtile(zTileW,zTileH,gndFillCol,gndLoCol,gndHiCol);
	var zPlaneTile = createZtile(zTileW,zTileH,planeFillCol,planeLoCol,planeHiCol);



	var xPlaneW = voxDim*drawGrid.yDim+1; // note: x width defined by yDim
	var xPlaneH = voxDim*drawGrid.zDim+voxDim*drawGrid.yDim/2+1; // x height defined by yDim+zDim
	var xPlaneCan = makeCanvas(xPlaneW,xPlaneH);
	var xPlaneCtx = xPlaneCan.getContext("2d");

	var yPlaneW = voxDim*drawGrid.xDim+1; // note: y width defined by xDim
	var yPlaneH = voxDim*drawGrid.zDim+voxDim*drawGrid.xDim/2+1; // y height defined by xDim+zDim
	var yPlaneCan = makeCanvas(yPlaneW,yPlaneH);
	var yPlaneCtx = yPlaneCan.getContext("2d");

	var zPlaneW = voxDim*(drawGrid.xDim+drawGrid.yDim)+1; // note: z width defined by xDim & yDim
	var zPlaneH = voxDim*(drawGrid.xDim+drawGrid.yDim)/2+1; // z height defined by xDim & yDim
	var zPlaneCan = makeCanvas(zPlaneW,zPlaneH);
	var zPlaneCtx = zPlaneCan.getContext("2d");


	// tile the tiles

	// x tile
	for (var y = 0;y<drawGrid.yDim/4;y++) {
		for (var z = 0;z<drawGrid.zDim/4;z++) {
			var cPt = voxPointToCanvas(new Point3(0,y*4,z*4));
			gndCtx.drawImage(xTile,cPt.x-xTileW-1,cPt.y-xTileW-1);
			xPlaneCtx.drawImage(xPlaneTile,y*voxDim*4,z*voxDim*4-y*voxDim*2+(drawGrid.yDim-4)*voxDim/2);
		}
	}
	// y tile
	for (var x = 0;x<drawGrid.xDim/4;x++) {
		for (var z = 0;z<drawGrid.zDim/4;z++) {
			var cPt = voxPointToCanvas(new Point3(x*4,0,z*4));
			gndCtx.drawImage(yTile,cPt.x-1,cPt.y-yTileW-1);
			yPlaneCtx.drawImage(yPlaneTile,x*voxDim*4,z*voxDim*4+x*voxDim*2);
		}
	}

	// z tile
	for (var x = 0;x<drawGrid.xDim/4;x++) {
		for (var y = 0;y<drawGrid.yDim/4;y++) {
			var cPt = voxPointToCanvas(new Point3(x*4,y*4,0));
			gndCtx.drawImage(zTile,cPt.x-zTileW/2-1,cPt.y-1);
			zPlaneCtx.drawImage(zPlaneTile, x*voxDim*4 - y*voxDim*4 + (drawGrid.xDim-4)*voxDim , x*voxDim*2 + y*voxDim*2 );
		}
	}
	gndImg = gndCan;

	xPlaneImg = xPlaneCan;
	yPlaneImg = yPlaneCan;
	zPlaneImg = zPlaneCan;
	
}
// resetGrid()
function resetGrid(kin) {
	// called whenever the drawGrid is zoomed or the voxGrid loads new data
	drawGrid.xDim = 320/voxDim;
	drawGrid.yDim = 320/voxDim;
	drawGrid.zDim = 320/voxDim;
	voxOffset = { x: -voxDim-1, y: -voxDim-1 };

	initVoxes();
	cursor = createVoxImage(new Color(0,0,0,255),voxDim,"cursor");

	createGnd();

	constrainDrawGrid();
	setRegions(kin);
	fullRedraw = true;
}

// main //
function main() {
	drawKin = new Kinetic_2d("drawgrid");
	var drawCan = drawKin.getCanvas();

	// kill the text cursor
	drawCan.onselectstart = function () { return false; } // ie
	drawCan.onmousedown = function () { return false; } // mozilla

	// init drag event handlers
	drawCan.addEventListener("dragenter", noopHandler, false);
	drawCan.addEventListener("dragexit", noopHandler, false);
	drawCan.addEventListener("dragover", noopHandler, false);
	drawCan.addEventListener("drop", handleImageDrop, false);

	window.addEventListener('keydown',handleKeyDn,true);
	window.addEventListener('keyup',handleKeyUp,true);

	// set dims
	centerPt.x = Math.round(drawCan.width/2);
	centerPt.y = Math.round(drawCan.height/2);

	mode = "loading";

	// init the sprites
	for (var i=0;i<spriteList.length;i++)
		spriteList[i].init();

	toolCursor = createVoxImage(new Color(0,0,0,255),10,"cursor");
	emptyVox = createVoxImage(new Color(255,255,255,255),10,"empty");
	resetGrid(drawKin);

	drawCan.onmouseup = function() {
			mode = "rollover";
	};

	drawKin.setDrawStage(function() {
		updateGrid(drawKin);
	});
	updateGrid(drawKin);

	// tools
	toolsKin = new Kinetic_2d("tools");

	toolsKin.setDrawStage(function(){
		updateTools(toolsKin);
	});
	updateTools(toolsKin);
}














